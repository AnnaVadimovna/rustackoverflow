package com.annab.finalapp.http;

import com.annab.finalapp.model.responces.QuestionsResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by abalabaichkina on 07.05.15.
 */
public interface QuestionService {
    @GET("/questions?order=desc&sort=activity&site=ru.stackoverflow&filter=withbody")
    void getQuestions(@Query("access_token")String accessToken,
                      @Query("key") String appKey,
                      Callback<QuestionsResponse> callback);

    @GET("/search?order=desc&sort=activity&site=ru.stackoverflow&filter=withbody")
    void getSearchQuestions(@Query("intitle") String intitle,
                            @Query("access_token")String accessToken,
                            @Query("key") String appKey,
                            Callback<QuestionsResponse> callback);
}
