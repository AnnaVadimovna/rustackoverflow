package com.annab.finalapp.http;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by abalabaichkina on 11.05.15.
 */
public interface LoginService {
    @FormUrlEncoded
    @POST("/access_token/")
    void requestAccessToken(@Field("client_id") String clientId,
                            @Field("client_secret") String clientSecret,
                            @Field("code") String code,
                            @Field("redirect_uri") String redirectUri,
                            @Field("client_secret") Callback<Response> callback);
}
