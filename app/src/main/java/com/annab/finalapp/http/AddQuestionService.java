package com.annab.finalapp.http;

import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;

import com.annab.finalapp.model.responces.QuestionsResponse;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by abalabaichkina on 12.05.15.
 */
public interface AddQuestionService {
    @FormUrlEncoded
    @POST("/questions/add")
    void addQuestion (@Field("title") String title,
                      @Field("body") String body,
                      @Field("tags") String tags,
                      @Field("access_token")String accessToken,
                      @Field("key") String appKey,
                      @Field("site") String site,
                      Callback<Response> callback
    );
  //  void getQuestions(Callback<QuestionsResponse> callback);


}
