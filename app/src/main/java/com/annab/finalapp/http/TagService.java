package com.annab.finalapp.http;

import com.annab.finalapp.model.responces.TagsResponse;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by abalabaichkina on 10.05.15.
 */
public interface TagService {
    @GET("/tags?order=desc&sort=popular&site=ru.stackoverflow")
    void getTags(Callback<TagsResponse> callback);
}
