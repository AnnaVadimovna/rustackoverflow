package com.annab.finalapp.http;

import com.annab.finalapp.model.responces.UserResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface UserService {
    @GET("/me?order=desc&sort=reputation&site=ru.stackoverflow")
    void getMe(@Query("access_token") String accessToken,
               @Query("key") String key,
               Callback<UserResponse> callback);


}
