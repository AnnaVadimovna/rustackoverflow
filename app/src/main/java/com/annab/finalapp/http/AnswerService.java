package com.annab.finalapp.http;

import com.annab.finalapp.model.responces.AnswersResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by abalabaichkina on 09.05.15.
 */
public interface AnswerService {
    @GET("/questions/{ids}/answers?order=desc&sort=activity&site=ru.stackoverflow&filter=withbody")
    void getAnswers(@Path("ids") long id,
                    @Query("access_token")String accessToken,
                    @Query("key") String appKey,
                    Callback<AnswersResponse> callback);

}
