package com.annab.finalapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.annab.finalapp.model.Account;
import com.annab.finalapp.model.Answer;
import com.annab.finalapp.model.Owner;
import com.annab.finalapp.model.Question;
import com.annab.finalapp.model.SimpleTag;
import com.annab.finalapp.model.User;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by abalabaichkina on 09.05.15.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();

    //имя файла базы данных который будет храниться в /data/data/APPNAME/DATABASE_NAME.db
    private static final String DATABASE_NAME = "myappname.db";

    //с каждым увеличением версии, при нахождении в устройстве БД с предыдущей версией будет выполнен метод onUpgrade();
    private static final int DATABASE_VERSION = 1;

    //ссылки на DAO соответсвующие сущностям, хранимым в БД
    private QuestionDao questionDao = null;
    private OwnerDao ownerDao = null;
    private AnswerDao answerDao = null;
    private AccountDao accountDao;
    private UserDao userDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Выполняется, когда файл с БД не найден на устройстве
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Question.class);
            TableUtils.createTable(connectionSource, Owner.class);
            TableUtils.createTable(connectionSource, Answer.class);
            TableUtils.createTable(connectionSource, Account.class);
            TableUtils.createTable(connectionSource, User.class);
        } catch (SQLException e) {
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }

    //Выполняется, когда БД имеет версию отличную от текущей
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer) {
        try {
            //Так делают ленивые, гораздо предпочтительнее не удаляя БД аккуратно вносить изменения
            TableUtils.dropTable(connectionSource, Question.class, true);
            TableUtils.dropTable(connectionSource, Owner.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "error upgrading db " + DATABASE_NAME + "from ver " + oldVer);
            throw new RuntimeException(e);
        }
    }

    //синглтон для QuestionDao
    public QuestionDao getQuestionDao() throws SQLException {
        if (questionDao == null) {
            questionDao = new QuestionDao(getConnectionSource(), Question.class);
        }
        return questionDao;
    }

    //синглтон для OwnerDao
    public OwnerDao getOwnerDao() throws SQLException {
        if (ownerDao == null) {
            ownerDao = new OwnerDao(getConnectionSource(), Owner.class);
        }
        return ownerDao;
    }

    public AnswerDao getAnswerDao() throws SQLException {
        if (answerDao == null) {
            answerDao = new AnswerDao(getConnectionSource(), Answer.class);
        }
        return answerDao;
    }

    public AccountDao getAccountDao() throws SQLException {
        if (accountDao == null) {
            accountDao = new AccountDao(getConnectionSource(), Account.class);
        }
        return accountDao;
    }

    public UserDao getUserDao() throws SQLException {
        if (userDao == null) {
            userDao = new UserDao(getConnectionSource(), User.class);
        }
        return userDao;
    }

    @Override
    public void close() {
        super.close();
        questionDao = null;
        ownerDao = null;
        answerDao = null;
        userDao = null;
        accountDao = null;
    }

}
