package com.annab.finalapp.db;

import com.annab.finalapp.model.Owner;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by abalabaichkina on 09.05.15.
 */
public class OwnerDao extends BaseDaoImpl<Owner, Integer>{
    protected OwnerDao(ConnectionSource connectionSource,
                      Class<Owner> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Owner> getAllOwners() throws SQLException{
        return this.queryForAll();
    }
}
