package com.annab.finalapp.db;

import com.annab.finalapp.model.Answer;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by abalabaichkina on 10.05.15.
 */
public class AnswerDao extends BaseDaoImpl<Answer, Long> {
    protected AnswerDao(ConnectionSource connectionSource, Class<Answer> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Answer> getAllAnswers() throws SQLException {
        return this.queryForAll();
    }


}
