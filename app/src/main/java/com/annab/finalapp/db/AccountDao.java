package com.annab.finalapp.db;

import android.content.Intent;

import com.annab.finalapp.model.Account;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by Георгий on 11.05.2015.
 */
public class AccountDao extends BaseDaoImpl<Account, Integer> {

    protected AccountDao(ConnectionSource connectionSource, Class<Account> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

}
