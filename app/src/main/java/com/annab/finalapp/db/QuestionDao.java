package com.annab.finalapp.db;

import com.annab.finalapp.model.Question;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by abalabaichkina on 09.05.15.
 */
public class QuestionDao extends BaseDaoImpl<Question, Long> {
    protected QuestionDao(ConnectionSource connectionSource,
                      Class<Question> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Question> getAllQuestions() throws SQLException{
        return this.queryForAll();
    }

    //public Question getQ()
}
