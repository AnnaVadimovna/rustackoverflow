package com.annab.finalapp.db;

import com.annab.finalapp.model.User;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by Георгий on 11.05.2015.
 */
public class UserDao extends BaseDaoImpl<User, Integer> {

    protected UserDao(ConnectionSource connectionSource, Class<User> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

}
