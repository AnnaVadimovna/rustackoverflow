package com.annab.finalapp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by abalabaichkina on 11.05.15.
 */
@DatabaseTable(tableName = "account")
public class Account {
    @DatabaseField(columnName = "_id", generatedId = true)
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @DatabaseField()
    String accessToken;

    @DatabaseField()
    String refreshToken;

    @DatabaseField(foreign = true)
    User user;


    public Account() {

    }

    public Account(String refreshToken, String accessToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
