package com.annab.finalapp.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by abalabaichkina on 05.05.15.
 */
@DatabaseTable(tableName = "answers")
public class Answer {
    @DatabaseField(foreign = true)
    Owner owner;
    @SerializedName("is_accepted")
    @DatabaseField()
    boolean isAccepted;
    @DatabaseField()
    int score;
    @SerializedName("last_activity_date")
    @DatabaseField()
    long lastActivityDate;
    @SerializedName("last_edit_date")
    @DatabaseField()
    long lastEditDate;
    @SerializedName("creation_date")
    @DatabaseField()
    long creationDate;
    @SerializedName("answer_id")
    @DatabaseField(id = true, columnName = "_id")
    long answerId;
    @SerializedName("question_id")
    @DatabaseField()
    long questionId;
    @DatabaseField()
    String body;

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getLastActivityDate() {
        return lastActivityDate;
    }

    public void setLastActivityDate(long lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }

    public long getLastEditDate() {
        return lastEditDate;
    }

    public void setLastEditDate(long lastEditDate) {
        this.lastEditDate = lastEditDate;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
