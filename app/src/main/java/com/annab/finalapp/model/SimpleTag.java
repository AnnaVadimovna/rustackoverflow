package com.annab.finalapp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Георгий on 12.05.2015.
 */
@DatabaseTable(tableName = "SimpleTags")
public class SimpleTag {
    @DatabaseField(generatedId = true, columnName = "_id")
    int id;
    @DatabaseField()
    String name;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
