package com.annab.finalapp.model.responces;

import com.annab.finalapp.model.Tag;

import java.util.List;

/**
 * Created by abalabaichkina on 10.05.15.
 */
public class TagsResponse {
    List<Tag> items;
    public List<Tag> getItems() {
        return items;
    }

    public void setItems(List<Tag> items) {
        this.items = items;
    }
}
