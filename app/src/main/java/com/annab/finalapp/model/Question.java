package com.annab.finalapp.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by abalabaichkina on 05.05.15.
 */

@DatabaseTable(tableName = "questions")
public class Question {
//    @ForeignCollectionField()
//    ForeignCollection<SimpleTag> tags;
    @DatabaseField(foreign = true)
    Owner owner;
    @SerializedName("is_answered")
    @DatabaseField()
    boolean isAnswered;
    @SerializedName("view_count")
    @DatabaseField()
    int viewCount;
    @SerializedName("answer_count")
    @DatabaseField()
    int answerCount;
    @DatabaseField()
    int score;
    @SerializedName("last_activity_date")
    @DatabaseField()
    long lastActivityDate;
    @SerializedName("creation_date")
    @DatabaseField()
    long creationDate;
    @SerializedName("last_edit_date")
    @DatabaseField()
    long lastEditDate;
    @SerializedName("question_id")
    @DatabaseField(id = true, columnName = "_id")
    long questionId;
    @DatabaseField()
    String link;
    @DatabaseField()
    String body;
    @DatabaseField()
    String title;

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

//    public List<String> getTags() {
//        return tags;
//    }
//
//    public void setTags(List<String> tags) {
//        this.tags = tags;
//    }

    public boolean isAnswered() {
        return isAnswered;
    }

    public void setIsAnswered(boolean isAnswered) {
        this.isAnswered = isAnswered;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public int getAnswerCount() {
        return answerCount;
    }

    public void setAnswerCount(int answerCount) {
        this.answerCount = answerCount;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getLastActivityDate() {
        return lastActivityDate;
    }

    public void setLastActivityDate(long lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



}
