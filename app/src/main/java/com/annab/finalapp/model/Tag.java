package com.annab.finalapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abalabaichkina on 10.05.15.
 */
public class Tag {
    @SerializedName("has_synonyms")
    boolean hasSynonyms;
    @SerializedName("is_moderator_only")
    boolean isModeratorOnly;
    @SerializedName("is_required")
    boolean isRequired;
    long count;
    String name;

    @Override
    public String toString() {
        return name;
    }
}
