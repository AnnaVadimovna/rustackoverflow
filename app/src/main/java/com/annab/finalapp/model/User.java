package com.annab.finalapp.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by  on 11.05.2015.
 */
@DatabaseTable(tableName = "USERS")
public class User {
    @DatabaseField(id = true, columnName = "_id")
    @SerializedName("user_id")
    long userId;
//    @DatabaseField()
//    @SerializedName("display_name")
//    String displayName;
//    @DatabaseField()
//    String link;
//    @DatabaseField()
//    @SerializedName("profile_image")
//    String profileImage;
//    @DatabaseField()
//    int reputation;
}
