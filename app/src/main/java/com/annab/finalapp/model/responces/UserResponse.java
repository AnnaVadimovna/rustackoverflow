package com.annab.finalapp.model.responces;

import com.annab.finalapp.model.User;

import java.util.List;

/**
 * Created by  on 11.05.2015.
 */
public class UserResponse {
    List<User> items;

    public List<User> getItems() {
        return items;
    }

    public void setItems(List<User> items) {
        this.items = items;
    }
}
