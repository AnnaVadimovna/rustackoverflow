package com.annab.finalapp.model.responces;

import com.annab.finalapp.model.Answer;

import java.util.List;

/**
 * Created by abalabaichkina on 09.05.15.
 */
public class AnswersResponse {
    List<Answer> items;
    public List<Answer> getItems() {
        return items;
    }

    public void setItems(List<Answer> items) {
        this.items = items;
    }

}
