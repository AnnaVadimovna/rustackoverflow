package com.annab.finalapp.model.responces;

import com.annab.finalapp.model.Question;

import java.util.List;

/**
 * Created by abalabaichkina on 07.05.15.
 */
public class QuestionsResponse {
    List<Question> items;
    public List<Question> getItems() {
        return items;
    }

    public void setItems(List<Question> items) {
        this.items = items;
    }

}
