package com.annab.finalapp.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by abalabaichkina on 05.05.15.
 */
public class Owner {
    @DatabaseField()
    int reputation;
    @SerializedName("user_id")
    @DatabaseField(id = true, columnName = "_id")
    long userId;
    @SerializedName("user_type")
    @DatabaseField()
    String userType;
    @SerializedName("profile_image")
    @DatabaseField()
    String profileImage;
    @SerializedName("display_name")
    @DatabaseField()
    String displayName;
    @DatabaseField()
    String link;
    @SerializedName("accept_rate")
    @DatabaseField()
    int acceptRate;
}
