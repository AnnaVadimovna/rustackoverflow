package com.annab.finalapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.annab.finalapp.Configures;
import com.annab.finalapp.R;
import com.annab.finalapp.adapters.AnswersAdapter;
import com.annab.finalapp.db.AnswerDao;
import com.annab.finalapp.db.HelperFactory;
import com.annab.finalapp.http.AnswerService;
import com.annab.finalapp.model.Account;
import com.annab.finalapp.model.Answer;
import com.annab.finalapp.model.Question;
import com.annab.finalapp.model.responces.AnswersResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abalabaichkina on 07.05.15.
 */
public class ViewQuestionActivity extends AppCompatActivity {
    private ListView listView;
    //private Ans questionsAdapter;
    List<Answer> answers = new ArrayList<>();
    private AnswerService answerService;
    private AnswersAdapter answersAdapter;
    private ShareActionProvider shareActionProvider;
    TextView questionText;
    TextView questionIdTxt;
    Toolbar toolbar;
    TextView answerCountTxt;

    Callback<AnswersResponse> answersResponseCallback = new Callback<AnswersResponse>() {
        @Override
        public void success(AnswersResponse answersResponse, Response response) {
            for (Answer item : answersResponse.getItems()) {
  /*              try {
                    HelperFactory.getHelper().getAnswerDao().create(item);
                } catch (SQLException e) {
                    e.printStackTrace();
                } */
                try {
                    AnswerDao answerDao = HelperFactory.getHelper().getAnswerDao();
                    if (answerDao.idExists(item.getAnswerId())) {
                        answerDao.update(item);
                    } else {
                        answerDao.create(item);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }

            answers.clear();
            answers.addAll(answersResponse.getItems());
            answersAdapter.notifyDataSetChanged();
        }

        @Override
        public void failure(RetrofitError retrofitError) {
            Log.i("rag", retrofitError.getMessage());
        }
    };
    private Question question;

    void requestAnswersFor(Question question) {
        Account account = null;
        try {
            account = HelperFactory.getHelper().getAccountDao().queryForAll().get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (account != null)
            answerService.getAnswers(question.getQuestionId(), account.getAccessToken(), Configures.AppKey, answersResponseCallback);
    }
/////////////////


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_view_question);
        toolbar = (Toolbar) findViewById(R.id.ac_view_question_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_arrow_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        listView = (ListView) findViewById(R.id.ac_view_question_listview);
        answersAdapter = new AnswersAdapter(this, answers);
        listView.setAdapter(answersAdapter);
//////
        long questionId = getIntent().getExtras().getLong("questionId");
         question = null;

        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.stackexchange.com")
                .build();

        answerService = restAdapter.create(AnswerService.class);

        try {
            question = HelperFactory.getHelper().getQuestionDao().queryForId(questionId);
            requestAnswersFor(question);
            answersAdapter.setQuestion(question);



//            String questionStr = question.getBody();
//            int answerCount = question.getAnswerCount();
//            questionText.setText(Html.fromHtml(questionStr));
//            long questionIdStr = question.getQuestionId();
//            questionIdTxt.setText(Long.toString(questionIdStr));
//            answerCountTxt.setText("Количество ответов: " + (Integer.toString(answerCount)));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_question, menu);

        MenuItem shareItem = menu.findItem(R.id.menu_item_share);
        shareActionProvider = new ShareActionProvider(this);
        MenuItemCompat.setActionProvider(shareItem, shareActionProvider);
//        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, question.getTitle());
        intent.putExtra(Intent.EXTRA_TEXT, question.getBody());
        setShareIntent(intent);
        return true;
    }

    private void setShareIntent(Intent shareIntent){
        if (shareActionProvider != null) {
            shareActionProvider.setShareIntent(shareIntent);
        }
    }

}

