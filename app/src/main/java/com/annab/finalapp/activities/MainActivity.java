package com.annab.finalapp.activities;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ShareActionProvider;

import com.annab.finalapp.Configures;
import com.annab.finalapp.R;
import com.annab.finalapp.adapters.QuestionsAdapter;
import com.annab.finalapp.db.HelperFactory;
import com.annab.finalapp.db.QuestionDao;
import com.annab.finalapp.http.QuestionService;
import com.annab.finalapp.model.Account;
import com.annab.finalapp.model.Question;
import com.annab.finalapp.model.responces.QuestionsResponse;
import com.melnykov.fab.FloatingActionButton;
import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private QuestionsAdapter questionsAdapter;
    List<Question> questions = new ArrayList<>();
    private QuestionService questionService;
    Toolbar toolbar;
    Drawer.Result drawerResult;
    SearchView searchView;
    FloatingActionButton addQuestionBtn;
    Callback<QuestionsResponse> callback = new Callback<QuestionsResponse>() {
        @Override
        public void success(QuestionsResponse questionsResponse, Response response) {
            for (Question item : questionsResponse.getItems()) {
                try {
                    QuestionDao questionDao = HelperFactory.getHelper().getQuestionDao();
                    if (questionDao.idExists(item.getQuestionId())) {
                        questionDao.update(item);
                    } else {
                        questionDao.create(item);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            questions.clear();
            questions.addAll(questionsResponse.getItems());
            questionsAdapter.notifyDataSetChanged();
        }

        @Override
        public void failure(RetrofitError retrofitError) {
            Log.i("rag", retrofitError.getMessage());
        }
    };
    private MenuItem searchItem;


    void addElement(String query) {
        Account account = null;
        try {
            account = HelperFactory.getHelper().getAccountDao().queryForAll().get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(account != null)
        questionService.getSearchQuestions(query, account.getAccessToken(), Configures.AppKey, callback);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.ac_main_toolbar);

        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        toolbar.setTitle(R.string.app_name);
//        initDrawer();

        listView = (ListView) findViewById(R.id.ac_main_listview);
        questionsAdapter = new QuestionsAdapter(this, questions);
        listView.setAdapter(questionsAdapter);

        addQuestionBtn = (FloatingActionButton) findViewById(R.id.ac_main_fab);
        addQuestionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewQuestion();
            }
        });

        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Configures.API_ROOT)
                .build();

        questionService = restAdapter.create(QuestionService.class);


        //itemClick
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("qqq", "itemClick: position = " + position + ", id = "
                        + id);
                newScreen(questionsAdapter.getItem(position));
            }
        });
        Account account = null;
        try {
            account = HelperFactory.getHelper().getAccountDao().queryForAll().get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(account != null)
        questionService.getQuestions(account.getAccessToken(), Configures.AppKey,callback);
    }

    private void addNewQuestion() {
        Intent intent = new Intent(this, AddQuestionActivity.class);
        startActivity(intent);
    }

    private void initDrawer() {
        drawerResult = new Drawer()
                .withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withHeader(R.layout.drawer_header)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_home).withIcon(FontAwesome.Icon.faw_home).withBadge("99").withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_free_play).withIcon(FontAwesome.Icon.faw_gamepad),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_custom).withIcon(FontAwesome.Icon.faw_eye).withBadge("6").withIdentifier(2),
                        new SectionDrawerItem().withName(R.string.drawer_item_settings),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_help).withIcon(FontAwesome.Icon.faw_cog),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_open_source).withIcon(FontAwesome.Icon.faw_question).setEnabled(false),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_contact).withIcon(FontAwesome.Icon.faw_github).withBadge("12+").withIdentifier(1)
                )
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        InputMethodManager inputMethodManager = (InputMethodManager) MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                    }

                    @Override
                    public void onDrawerSlide(View view, float v) {

                    }
                })
                .build();
    }

    public void newScreen(Question question) {
        Intent intObj = new Intent(this, ViewQuestionActivity.class);
        intObj.putExtra("questionId", question.getQuestionId());
        startActivity(intObj);
    }

    public static void open(AppCompatActivity activity) {
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_add_question) {
//            Intent intObj = new Intent(this, AddQuestionActivity.class);
//            startActivity(intObj);
//            return true;
//        }
//
//        if (id == R.id.action_login) {
//            Intent intObj = new Intent(this, LoginActivity.class);
//            startActivity(intObj);
//            return true;
//        }
//
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            MenuItemCompat.collapseActionView(searchItem);
            searchView.onActionViewCollapsed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

//        MenuItem shareItem = menu.findItem(R.id.menu_item_share);
//        shareActionProvider = (ShareActionProvider) shareItem.getActionProvider();


        searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

//        SearchManager searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);
//
//        searchView = null;
//        if (searchItem != null) {
//            searchView = (SearchView) searchItem.getActionView();
//        }
//        if (searchView != null) {
//            searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
//        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.i("tag", query);
            MenuItemCompat.collapseActionView(searchItem);
            searchView.onActionViewCollapsed();
            addElement(query);
            //use the query to search your data somehow
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setShareIntent(Intent shareIntent){
//        if (shareActionProvider !=null){
//            shareActionProvider.setShareIntent(shareIntent);
//        }
    }
}
