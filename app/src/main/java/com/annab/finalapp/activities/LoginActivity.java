package com.annab.finalapp.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.annab.finalapp.Configures;
import com.annab.finalapp.R;
import com.annab.finalapp.db.HelperFactory;
import com.annab.finalapp.http.LoginService;
import com.annab.finalapp.http.UserService;
import com.annab.finalapp.model.Account;
import com.annab.finalapp.model.responces.UserResponse;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends AppCompatActivity {
    WebView webView;
    WebChromeClient webChromeClient;
    WebViewClient webViewClient;
    LoginService loginService;
    UserService userService;
    Toolbar toolbar;

    private final static String url = "https://stackexchange.com/oauth?client_id=4754&redirect_uri=http://localhost&scope=no_expiry,private_info,write_access";
    private Callback<Response> responseCallback;
    private Callback<UserResponse> userCallback = new Callback<UserResponse>() {
        @Override
        public void success(UserResponse userResponse, Response response) {
            //create account
            if (userResponse.getItems().size() == 0) {
                showRuStackWarning();
                return;
            }
            Account account = new Account(refreshToken, accessToken);
            account.setUser(userResponse.getItems().get(0));
            try {
                HelperFactory.getHelper().getAccountDao().create(account);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            MainActivity.open(LoginActivity.this);
            finish();
        }

        @Override
        public void failure(RetrofitError error) {

        }
    };

    private void showRuStackWarning() {
        Dialog dialog = new Dialog(this);
        dialog.setTitle(R.string.ru_stack_user_not_find);
        dialog.show();
        startLogin();
    }

    private String refreshToken;
    private String accessToken;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<Account> accounts = null;
        try {
            accounts = HelperFactory.getHelper().getAccountDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (accounts != null && accounts.size() != 0) {
            MainActivity.open(this);
            finish();
            return;
        }
        setContentView(R.layout.ac_login);
        toolbar = (Toolbar) findViewById(R.id.ac_login_toolbar);
        toolbar.setTitle(R.string.title_activity_login);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.title_activity_login);
        webView = (WebView) findViewById(R.id.ac_login_webview);
        webChromeClient = new WebChromeClient();
        webViewClient = new MyWebViewClient();
        webView.setWebChromeClient(webChromeClient);
        webView.setWebViewClient(webViewClient);
        webView.getSettings().setJavaScriptEnabled(true);
        startLogin();

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("https://stackexchange.com/oauth/").build();
        loginService = restAdapter.create(LoginService.class);
        restAdapter = new RestAdapter.Builder().setEndpoint(Configures.API_ROOT).build();
        userService = restAdapter.create(UserService.class);
        responseCallback = new Callback<Response>() {
            @Override
            public void success(Response accessTokenResponse, Response response) {
                Log.i("tag", "success");
                String result = null;
                try {
                    result = IOUtils.toString(response.getBody().in());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.i("result", result);
                if (result != null && result.startsWith("access_token=")) {
                    accessToken = result.substring("access_token=".length());
                    Log.i("accessToken", accessToken);
                    userService.getMe(accessToken, Configures.AppKey, userCallback);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.i("tag", "failure");
            }
        };
    }

    private void startLogin() {
        CookieManager cookieManager = CookieManager.getInstance();

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP){
            cookieManager.removeAllCookies(null);
            // Do something for froyo and above versions
        } else{
            // do something for phones running an SDK before froyo
            cookieManager.removeAllCookie();
        }


        WebSettings ws = webView.getSettings();
        ws.setSaveFormData(false);

        webView.clearHistory();
        webView.clearFormData();
        webView.clearCache(true);
        webView.loadUrl(url);
    }

    class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("http://localhost")) {
                getCodeFromUrl(url);
                return true;
            }
            return false;
        }

    }

    private void getCodeFromUrl(String url) {
        Log.i("url", url);
        if (url.startsWith("http://localhost/?code=")) {
            refreshToken = null;
            try {
                Map<String, String> map = splitQuery(url);
                refreshToken = map.get("code");

            } catch (UnsupportedEncodingException | MalformedURLException e) {
                e.printStackTrace();
            }
            if (refreshToken != null) {
                loginService.requestAccessToken(Configures.ClientId, Configures.ClientSecret, refreshToken, "http://localhost", responseCallback);
            }
        }

    }

    public static Map<String, String> splitQuery(String url) throws UnsupportedEncodingException, MalformedURLException {
        URL u = new URL(url);
        Map<String, String> query_pairs = new LinkedHashMap<>();
        String query = u.getQuery();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }
}
