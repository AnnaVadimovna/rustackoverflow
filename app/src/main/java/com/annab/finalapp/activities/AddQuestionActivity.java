package com.annab.finalapp.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.annab.finalapp.Configures;
import com.annab.finalapp.R;
import com.annab.finalapp.db.HelperFactory;
import com.annab.finalapp.http.AddQuestionService;
import com.annab.finalapp.http.TagService;
import com.annab.finalapp.model.Account;
import com.annab.finalapp.model.Tag;
import com.annab.finalapp.model.responces.TagsResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AddQuestionActivity extends ActionBarActivity {
    MultiAutoCompleteTextView autoTags;
    EditText titleEt;
    EditText bodyEt;
    Button addBtn;
    List<Tag> tags = new ArrayList<Tag>();
    private AddQuestionService addQuestionService;
    private ArrayAdapter<Tag> adp;
    private TagService tagService;
    Toolbar toolbar;
    private String titleStr;
    private String bodyStr;
    private String autoTagsStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_add_question);
        toolbar = (Toolbar)findViewById(R.id.ac_add_question_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_arrow_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        autoTags = (MultiAutoCompleteTextView) findViewById(R.id.ac_add_question_tags);
        titleEt = (EditText) findViewById(R.id.ac_add_question_title);
        bodyEt = (EditText) findViewById(R.id.ac_add_question_body);
        addBtn = (Button) findViewById(R.id.ac_action_add);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addQuestion();
            }
        });

        autoTags.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

         adp=new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, tags);

        autoTags.setThreshold(1);
        autoTags.setAdapter(adp);

        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Configures.API_ROOT)
                .build();

        tagService = restAdapter.create(TagService.class);
        prepareTags();

        addQuestionService = restAdapter.create(AddQuestionService.class);



        //addQuestion();

    }

    private void addQuestion() {
        titleStr = titleEt.getText().toString();
        bodyStr = bodyEt.getText().toString();
        autoTagsStr = autoTags.getText().toString();
        Account account = null;
        try {
            account = HelperFactory.getHelper().getAccountDao().queryForAll().get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (account != null)
            addQuestionService.addQuestion(titleStr, bodyStr, autoTagsStr, account.getAccessToken(), Configures.AppKey,"ru.stackoverflow", new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    Log.i("tag", "success" );
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.i("tag failure", error.getMessage());
                }
            });
    }



//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_add_question, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    public void prepareTags(){
        Callback<TagsResponse> callback = new Callback<TagsResponse>() {
            @Override
            public void success(TagsResponse tagsResponse, Response response) {
                for (Tag item:tagsResponse.getItems()){
//                    try {
//                        HelperFactory.getHelper().getQuestionDao().create(item);
//                    } catch (SQLException e) {
//                        e.printStackTrace();
//                    }
                }

                tags.clear();
                tags.addAll(tagsResponse.getItems());
                adp.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i("rag", retrofitError.getMessage());
            }
        };
        tagService.getTags(callback);
    }


}
