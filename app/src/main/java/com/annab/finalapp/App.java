package com.annab.finalapp;

import android.app.Application;

import com.annab.finalapp.db.HelperFactory;

/**
 * Created by abalabaichkina on 09.05.15.
 */
public class App extends Application{
    @Override
    public void onCreate(){
        super.onCreate();
        HelperFactory.setHelper(getApplicationContext());
    }

    @Override
    public void onTerminate(){
        HelperFactory.releaseHelper();
        super.onTerminate();
    }

}
