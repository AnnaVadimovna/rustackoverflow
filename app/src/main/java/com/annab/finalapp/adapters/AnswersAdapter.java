package com.annab.finalapp.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.annab.finalapp.R;
import com.annab.finalapp.model.Answer;
import com.annab.finalapp.model.Question;

import java.util.List;

/**
 * Created by abalabaichkina on 10.05.15.
 */
public class AnswersAdapter extends BaseAdapter {
    LayoutInflater layoutInflater;
    List<Answer> answers;
    Question question;
    private String content;
    private TextView questionText;
    private String bodyStr;

    public void setQuestion(Question question) {
        this.question = question;
    }


///////////////////  добавила этот метод. Надо ли?!
//    public QuestionsAdapter(Context context, Question question) {
//        this.layoutInflater = LayoutInflater.from(context);
//        this.question = question;
//    }

    public AnswersAdapter(Context context, List<Answer> answers) {
        this.layoutInflater = LayoutInflater.from(context);
        this.answers = answers;
    }

    @Override
    public int getCount() {
        return answers.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        if (position == 0)
            return question;
        return answers.get(position - 1);
        // question.get(position);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public long getItemId(int position) {
        Object o = getItem(position);
        if(o instanceof Question){
            return 0;
        }
        return 1;
    }

    static class ViewHolder {
        TextView title;
        TextView body;
        TextView answerCount;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = null;
        ViewHolder viewHolder; //=null
        int type = (int) getItemId(position);
        if (type == 0) {
            return getQustionView(position, convertView, parent);
        } else {
            return getAnswerView(position, convertView, parent);
        }


    }

    private View getQustionView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        convertView = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();//+
            convertView = layoutInflater.inflate(R.layout.item_question_description, parent, false);
            viewHolder.title = (TextView) convertView.findViewById(R.id.item_question_desc_title);
            viewHolder.body = (TextView) convertView.findViewById(R.id.item_question_desc_questionbody);
            viewHolder.answerCount = (TextView) convertView.findViewById(R.id.item_question_desc_answercount);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Question q = (Question) getItem(position);

        content = q.getTitle();
        bodyStr = q.getBody();
        viewHolder.title.setText(Html.fromHtml(content));
        viewHolder.body.setText(Html.fromHtml(bodyStr));
        viewHolder.answerCount.setText("Количество ответов: " + Integer.toString(q.getAnswerCount()));


        return convertView;
    }

    private View getAnswerView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        convertView = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();//+
            convertView = layoutInflater.inflate(R.layout.item_answer_description, parent, false);
            viewHolder.title = (TextView) convertView.findViewById(R.id.item_answer_desc_title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Answer a = (Answer) getItem(position);
        content = a.getBody();
        viewHolder.title.setText(Html.fromHtml(content));
        return convertView;
    }

}

