package com.annab.finalapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.annab.finalapp.R;
import com.annab.finalapp.model.Question;

import java.util.List;

/**
 * Created by abalabaichkina on 06.05.15.
 */
public class QuestionsAdapter extends BaseAdapter {
    LayoutInflater layoutInflater;
    List<Question> questions;

    public QuestionsAdapter(Context context, List<Question> questions) {
        this.layoutInflater = LayoutInflater.from(context);
        this.questions = questions;
    }

    @Override
    public int getCount() {
        return questions.size();
    }

    @Override
    public Question getItem(int position) {
        return questions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    static class ViewHolder{
        TextView title;
        TextView tags;
        TextView answersCount;
        TextView qScore;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null){
            convertView=layoutInflater.inflate(R.layout.item_question, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.title= (TextView) convertView.findViewById(R.id.item_question_title);
            viewHolder.tags= (TextView) convertView.findViewById(R.id.item_question_tags);
            viewHolder.answersCount= (TextView) convertView.findViewById(R.id.item_question_answerscount);
            viewHolder.qScore = (TextView) convertView.findViewById(R.id.item_question_score);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.title.setText(getItem(position).getTitle());
        viewHolder.answersCount.setText(Integer.toString(getItem(position).getAnswerCount()));
        viewHolder.qScore.setText(Integer.toString(getItem(position).getScore()));
        return convertView;
    }
}
